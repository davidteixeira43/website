// import '@babel/polyfill';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import VueYouTubeEmbed from 'vue-youtube-embed';

Vue.config.productionTip = false;

Vue.use(VueYouTubeEmbed);

const app = new Vue({
    router,
    render: h => h(App),
    vuetify
}).$mount('#app');
if (window.Cypress) {
    window.app = app;
}
